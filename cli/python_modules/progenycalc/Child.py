# add module dir
import sys
sys.path.append('./python_modules/DNA')

from Person import Person

class Child(Person):
    """
    This class is used for defining a child and stores
    basic genetic data and can create itself out of two gametes with
    the init function
    """
    def __init__(self, ovum, sperm, handy, html_state, prog_calc):
        # make genotype out of ovum and sperm
        self.genotype = []
        # gamete length (amount of alleles)
        gametes_comb_len = int((len(ovum)+len(sperm))/2)
        # merge parents gametes to create own genotype
        for i in range(gametes_comb_len):
            gene = ovum[i]+sperm[i]
            gene = "".join(sorted(gene, key=lambda x: (x.lower(), x)))
            self.genotype.append(gene)
        # fix up genotype with join
        self.genotype = "".join(self.genotype)
        # genotype defined, run person init
        Person.__init__(self, self.genotype, handy, "Unknown", html_state, \
        prog_calc)
        # !!!
        # should not define child.fenotypes because it is more efficient to
        # get fenotypes for reoccuring genotypes. Else the program
        # is going through many uneccesary loops
        # but I apparently have to, define self.fenotype:
        self.getGenes()
        self.getFenotype()

