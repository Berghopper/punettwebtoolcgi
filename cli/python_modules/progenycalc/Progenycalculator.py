import sys
import time
sys.path.append('./python_modules/progenycalc')
from Adult import Adult

class ProgenyCalculator():
    """
    This class defines ProgenyCalculator and calculates all possible
    geno- and fenotypes of the next generation with the given
    genotypes of parents.
    """
    def __init__(self, *args):
        """
        Init basically calculates everything and makes two important
        variables:
        self.finished_data
        self.calculation_time
        for cli you could print the data by calling the following:
        ProgenyCalculator.print_Results()
        for other outside programs you could just easily call this
        class and get the data with the finished_data variable
        """
        # time the calculation
        begin = time.time()
        # define mom, dad, labels and the html handler_object
        self.mom, self.dad, self.labels, self.handy, self.html = args
        # when html is true, run this part of init
        if self.html:
            # check for labels
            if not self.labels:
                # if labels aren't given, don't calculate yet.
                self.mom = Adult(self.mom, self.handy, "female", self.html, \
                self)
                self.dad = Adult(self.dad, self.handy, "male", self.html, self)
                # check if dad and mom correspond with eachother and define
                # self.comb_len (combined length)
                self.genotypesCorrespond()
            else:
                self.mom = Adult(self.mom, self.handy, "female", self.html, \
                self)
                self.dad = Adult(self.dad, self.handy, "male", self.html, self)
                # check if dad and mom correspond with eachother and define
                # self.comb_len (combined length)
                self.genotypesCorrespond()
                # if they do, continue to define labels
                self.labelParser()
                # calculate possible sequences and define self.sequences
                self.gamete_sequence_generator()
                # get gametes of both parents (self.gametes)
                self.mom.getGametes(self.sequences)
                self.dad.getGametes(self.sequences)
                # get children
                self.possible_children = \
                self.mom.haveUnsafeSexWith(self.dad)
                # set genotype and fenotype amount dicts (child)
                # self.genotypes_dict
                # self.fenotypes_dict
                self.getGenotypesAsDict()
                self.getFenotypesAsDict()
                # calculate chances with the newly made dictionaries
                # updating them by adding percentages
                self.calculate_chances()
                # grab labels for fenotype if labels excist
                self.get_labels()
                # finish data, by sorting the dicionaries to lists
                self.finish_data()
        # else run the cli variant
        else:
            self.mom = Adult(self.mom, self.handy, "female", self.html, self)
            self.dad = Adult(self.dad, self.handy, "male", self.html, self)
            # check if dad and mom correspond with eachother and define
            # self.comb_len (combined length)
            self.genotypesCorrespond()
            # if they do, continue to define labels
            self.labelParser()
            # calculate possible sequences and define self.sequences
            self.gamete_sequence_generator()
            # get gametes of both parents (self.gametes)
            self.mom.getGametes(self.sequences)
            self.dad.getGametes(self.sequences)
            # get children
            self.possible_children = \
            self.mom.haveUnsafeSexWith(self.dad)
            # set genotype and fenotype amount dicts (child)
            # self.genotypes_dict
            # self.fenotypes_dict
            self.getGenotypesAsDict()
            self.getFenotypesAsDict()
            # calculate chances with the newly made dictionaries
            # updating them by adding percentages
            self.calculate_chances()
            # grab labels for fenotype if labels excist
            if self.labels:
                self.get_labels()
            # finish data, by sorting the dicionaries to lists
            self.finish_data()
        # calculate calculation time and safe it.
        self.calculation_time = round(time.time()-begin, 5)

    def calculate_chances(self):
        """
        Calculate chances of all genotypes and fenotypes
        """
        # calculate the amount of possible genotypes and fenotypes first
        possible_amount = 2**self.comb_len
        # genotype
        # each genotype chance is the same for each 1 combination
        # because there is an equal amount
        genotype_chance = (1/possible_amount)*100
        # calc all chances, save in dictionary
        for genotype, data in self.genotypes_dict.items():
            amount = data[0]
            chance = round(genotype_chance*amount, 5)
            self.genotypes_dict[genotype].append(chance)
        # fenotype
        # each fenotype chance is the same for each 1 combination
        # because there is an equal amount
        fenotype_chance = (1/possible_amount)*100
        # calc all chances, save in dictionary
        for fenotype, data in self.fenotypes_dict.items():
            amount = data[0]
            chance = round(fenotype_chance*amount, 5)
            self.fenotypes_dict[fenotype].append(chance)

    def finish_data(self):
        """
        sort both dictionaries to be representable.
        (genotypes_dict and fenotypes_dict)
        """
        genotypes = sorted(self.genotypes_dict.items(),\
            key=lambda dict_set: dict_set[1][0])
        fenotypes = sorted(self.fenotypes_dict.items(),\
            key=lambda dict_set: dict_set[1][0])
        # set finished data variable
        self.finished_data = (genotypes, fenotypes)

    def getGenotypesAsDict(self):
        """
        Grabs all child genotypes and puts them in a dictionary
        key = genotype child
        val = data (amount, chance)
        """
        # for each genotype of a child, put it in child_genotypes
        child_genotypes = \
        [child.genotype for child in self.possible_children]
        # make a genotype:data set for making the dictionary set
        self.genotypes_dict = \
        {genotype:[0] for genotype in child_genotypes}
        # filter out any duplicates with the amount data
        for genotype in child_genotypes:
            try:
                self.genotypes_dict[genotype][0] += 1
            except KeyError:
                if self.html:
                    sys.exit(self.handy.error_print(4))
                else:
                    sys.exit(self.error_print(4))

    def getFenotypesAsDict(self):
        """
        Grabs all child genotypes and puts them in a dictionary
        key = fenotype child
        val = data (amount, chance, labels)
        """
        # for each fenotype of a child, put it in child_fenotypes
        child_fenotypes = \
        [child.fenotype for child in self.possible_children]
        # make a fenotype:data set for making the dictionary set
        self.fenotypes_dict = \
        {fenotype:[0] for fenotype in child_fenotypes}
        # filter out any duplicates with the amount data
        for fenotype in child_fenotypes:
            try:
                self.fenotypes_dict[fenotype][0] += 1
            except KeyError:
                if self.html:
                    sys.exit(self.handy.error_print(5))
                else:
                    sys.exit(self.error_print(5))

    def cross_genotype_checker(self):
        """
        checks if both genotypes can be crossed with each other
        (if they are equal)
        """
        gen1 = self.mom.genotype
        gen2 = self.dad.genotype
        # if lengths are same, continue
        if len(gen1) == len(gen2):
            for i in range(int(self.comb_len)):
                # if each letter corresponds with each other, continue
                # upper or lower case doesn't matter
                if gen1[i] == gen2[i] or gen1[i].upper() == gen2[i] or \
                gen1[i] == gen2[i].upper():
                    booly = True
                else:
                    booly = False

            return booly
        else:
            return False

    def genotypesCorrespond(self):
        """
        uses cross_genotype_checker to check if the genotypes correspond
        """
        # if genotypes correspond, continue
        self.comb_len = \
        (len(self.mom.genotype)+len(self.dad.genotype))/2
        if self.cross_genotype_checker():
            pass
        # else print error
        else:
            if self.html:
                sys.exit(self.handy.error_print(6))
            else:
                sys.exit(self.error_print(6))

    def gamete_sequence_generator(self):
        """makes binary sequences for indexing all possible gametes"""
        # make a default binary string and make it the length of the
        # amount of allels
        bin_str = ""
        self.sequences = []
        for i in range(int(self.comb_len/2)):
            bin_str += '1'
        # make all possible combinations from 0 to max n-bit amount
        max_val = int(bin_str, 2)
        for i in range(0, max_val+1):
            self.sequences.append(("{:>"+str(int(self.comb_len/2))+"}")\
            .format(bin(i)[2:]).replace(" ", "0"))
        # each bit in a sequence represents the index number of 1 allele
        # 0  1  |
        # AaBb  |>Ab

    def labelParser(self):
        """
        saves labels (or traits) like a dictionary;
        {Aa: (haircolor, {a:blonde, A:brown}), ...}
        """
        # if labels doesn't exist, don't do anything.
        if not self.labels:
            pass
        else:
            # example of input: 'Aa=haircolor: a=blonde, A=brown; Bb...'
            # get genes and alleles of one of the parents and use it to
            # check the
            # input later on.
            trait_dict = {gene[0].upper()+gene[0].lower():\
            (gene[0].upper(), gene[1].lower())\
            for gene in self.mom.genes}
            # try to make a dictionary with the given string
            try:
                self.labels_dict = {}
                # for each set in labels, define stuff
                for a_set in self.labels.split(";"):
                    # define sets by splitting
                    gene_set, alleles_set = a_set.split(":")
                    # define gene and trait out of gene_set
                    gene, trait = gene_set.split("=")
                    gene = gene.strip()
                    # make subdict for alleles_set
                    allele_props = {}
                    # unpack allele and it's property
                    for allele_set in alleles_set.split(","):
                        allele, a_property = allele_set.split("=")
                        # append to allel property dictionary
                        allele_props[allele.strip()] = \
                        a_property.strip()
                    # combine the whole dictionary together
                    self.labels_dict[gene[0].upper()+gene[0].lower()] =\
                    (trait, allele_props)
            except (ValueError, KeyError) as error:
                if self.html:
                    sys.exit(self.handy.error_print(7))
                else:
                    sys.exit(self.error_print(7))
            # now check the dictionary and tell user if their input was
            # correct or not.
            for key in trait_dict.keys():
                try:
                    traits = self.labels_dict[key]
                except KeyError:
                    if self.html:
                        sys.exit(self.handy.error_print(8))
                    else:
                        sys.exit(self.error_print(8))
                try:
                    for allele in trait_dict[key]:
                        traits[1][allele]

                except KeyError:
                    if self.html:
                        sys.exit(self.handy.error_print(9))
                    else:
                        sys.exit(self.error_print(9))

    def get_labels(self):
        """
        This function is used for grabbing labels out of the generated
        label dictionary made with the labelparser
        """
        for fenotype in self.fenotypes_dict.keys():
            # give label for each trait
            full_label = []
            for allele in fenotype:
                key = allele.upper()+allele.lower()
                properties = self.labels_dict[key]
                trait = properties[0]
                a_property = properties[1][allele]
                full_label.append(trait+"= "+a_property)
            full_label = " , ".join(full_label)
            # append the label to corresponding fenotype in the dataset
            self.fenotypes_dict[fenotype].append(full_label)
    
    def error_print(self, errorcode):
        '''
        print error codes with a dict
        error codes:
        1 = something went wrong
        2, 3, 6 = something wrong with parent
        4, 5, 7, 8, 9 = something wrong with calculating
        '''
        errorcodes = {
        1: 'Oops!, something went wrong',
        2: 'One or more of the genotypes are incorrect.',
        3: 'You cannot cross two adults of '\
            'the same gender!',
        4: 'no match in gendict',
        5: 'no match in fendict',
        6: 'The genotypes don\'t '\
           'correspond, make sure they do.',
        7: 'error sorting labels, are you '\
            'sure your input is correct?',
        8: 'error sorting labels, missing '\
           'or incorrect gene',
        9: 'error sorting labels, missing '\
           'or incorrect traits',
        }
        # just print the error
        print(errorcodes[errorcode])

    def print_Results(self):
        """
        formats results in a representable way and prints them
        for the cli program
        """
        # grab results
        genotypes, fenotypes = self.finished_data
        # format certain strings to fit properly
        format_str_gen = "| occurances: {:<"+str(len((str(\
        fenotypes[-1][1][0]))))+"}"
        format_str_fen = "| occurances: {:<"+str(len((str(\
        fenotypes[-1][1][0]))))+"}"
        for item in genotypes:
            genotype = item[0]
            amount = item[1][0]
            chance = item[1][1]
            print("Genotype:", genotype, format_str_gen.format(amount),\
            "| probability:", str(chance)+"%")
        # print fenotypes
        if self.labels:
            # print with labels because they are given
            for item in fenotypes:
                fenotype = item[0]
                amount = item[1][0]
                chance = item[1][1]
                format_str_fen2 = "| probability: {:<"+str(len((str(\
                fenotypes[-1][1][1]))))+"}%"
                labels = item[1][2]
                print("Fenotype:", fenotype, format_str_fen.format(\
                amount), format_str_fen2.format(str(chance)), \
                "| traits:", labels)
            # print time
            print("Calculation time: "+str(self.calculation_time)+"s")
        else:
            # print without labels.
            for item in fenotypes:
                fenotype = item[0]
                amount = item[1][0]
                chance = item[1][1]
                print("Fenotype:", fenotype, format_str_fen.format(\
                amount), "| probability:", str(chance)+"%")
            # print time
            print("Calculation time: "+str(self.calculation_time)+"s")
