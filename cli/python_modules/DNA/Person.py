import string
import re
import sys

class Person():
    """
    This class holds basic genetic data of a person
    """
    def __init__(self, raw_string, handy, gender, html_state, prog_calc):
        """
        defines input and gender, rest has to be done manually in
        classes child and adult
        """
        self.raw_string = raw_string
        self.gender = gender
        self.handy = handy
        self.html_state = html_state
        self.prog_calc = prog_calc

    def single_genotype_checker(self):
        """
        checks if given string is a correct genotype
        """
        # check if genotype is an even number
        if len(self.raw_string)%2 == 0:
            # check if genotype is an alphanumeric sequence
            if re.fullmatch("[A-Za-z]+", self.raw_string):
                # sort genotype in alphanumeric order
                self.raw_string = list(self.raw_string)
                # use the lambda so it doesn't distinguish lowercase
                # from uppercase letters
                self.raw_string.sort(key=lambda x: x.upper())
                self.raw_string = "".join(self.raw_string)
                # define self.genes and check them
                self.getGenes()
                for gene in self.genes:
                    # check if the grouping of the 2 are matched
                    # (aA)=True
                    if gene[0] == gene[1].upper() or \
                    gene[0] == gene[1].lower() or \
                    gene[0] == gene[1]:
                        booly = True
                    else:
                        return False
                # for each letter in the alphabet count how many of each
                # letter
                # there is, if it is bigger than 2, false
                for letter in string.ascii_uppercase:
                    listy2 = list(map(lambda x: x.upper(),\
                    list(self.raw_string)))
                    if listy2.count(letter) > 2:
                        booly = False
                return booly
            else:
                return False
        else:
            return False

    def isValidGenotype(self):
        """
        Uses the single_genotype_checker to get a boolean back
        of the raw input to decide if input is correct
        only useful in parent because it checks input
        """
        # if true, define genotype and continue
        if self.single_genotype_checker():
            self.genotype = self.raw_string
        # else, exit the program with an error print
        else:
            if self.html_state:
                sys.exit(self.handy.error_print(2))
            else:
                sys.exit(self.prog_calc.error_print(2))

    def getFenotype(self):
        """
        Grabs fenotype and defines of self, would only be useful in
        parent, because in child it would make uneccesary loops
        (because you can count up the amount of double genotypes and
        go from there, it's more efficient.)
        before this function can be called, self.genes has to be defined
        """
        # define fenotype.
        self.fenotype = []
        # uses self.genes to decide which allele is expressed.
        for gene in self.genes:
            if not gene.islower():
                self.fenotype.append(gene[0].upper())
            else:
                self.fenotype.append(gene[0])
        # fix up self.fenotype with join
        self.fenotype = "".join(self.fenotype)

    def getGenes(self):
        """
        grabs and defines genes, isValidGenotype has to be run first
        if you're working with an input!
        else the program might crash.
        """
        # the reason self.raw_string is used instead of self.genotype
        # is because single_genotype_checker uses this function while
        # self.genotype is not defined yet.
        self.genes = []
        # walk trough string in increments of two to get alleles out.
        for i in range(0, len(self.raw_string), 2):
            self.genes.append(self.raw_string[i]+self.raw_string[i+1])
