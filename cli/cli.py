#!/usr/bin/env python3
# imports
import sys
import argparse
from python_modules.progenycalc import ProgenyCalculator
# constants
# classes
# functions
def argparse_setup():
    """
    sets up argsparse and returns arguments to main
    The argparse makes it easier to enter inputs in the terminal.
    """
    # initiate the ArgumentParser class
    parser = argparse.ArgumentParser()
    # add arguments to the object Argparse
    parser.add_argument("-p", "--papa", help="father genotype", \
    required=True)
    parser.add_argument("-m", "--mama", help="mother genotype", \
    required=True)
    parser.add_argument("-l", "--labels", help="labels for each gene "\
    "and allele example input:\n "\
    "'Aa=haircolor: a=blonde, A=brown; Bb...'", required=False)
    args = parser.parse_args()
    # define loose args
    dad = args.papa
    mom = args.mama
    labels = args.labels
    handy = None
    html = False
    # return correct sys.argv list
    return [sys.argv[0], mom, dad, labels, handy, html]


def main(argv=None):
    if argv==None:
        argv=sys.argv
    # work
    calcy = ProgenyCalculator(*argv[1:])
    calcy.print_Results()
    return 0

# initiate
if __name__=="__main__":
    sys.exit(main(argparse_setup()))
