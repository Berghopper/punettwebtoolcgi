/*
 * some of these functions have been grabbed from the magical place that is the
 * internet, I DID NOT make these functions myself, I only slightly edited them
 * so they would fit my website
 * sources:
 * http://papermashup.com/read-url-get-variables-withjavascript/
 * http://stackoverflow.com/questions/9136261/how-to-make-a-setinterval-stop-after-some-time-or-after-a-number-of-actions
*/


// function to grab url values 
function getUrlVars() {
var vars = {};
var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
vars[key] = value;
});
return vars;
}

// submit form automatically
document.getElementById("invisform").submit();
// loading bar 
var loader_an = document.getElementById("loader_an")
var stepsize = getUrlVars()["load_time"]*10
var i = 0;
// for interval stepsize, add 1 to percentage.
// stepsize defines how long loading takes.
var interval = setInterval(function(){ 
    if (i == 100) clearInterval(interval);
    loader_an.innerHTML = i+"% loading, please wait..."; 
    i++;
}, 
stepsize);
