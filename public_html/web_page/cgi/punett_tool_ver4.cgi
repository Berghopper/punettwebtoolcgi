#!/usr/bin/env python3
'''Punett webtool V2, calculates geno/fenotype for f1 of two parents
-fixed pep8 lay out (mostly)
-made the script a bit more logically structured
-fixed bug wherin scriptname is static and with different name chrashes.
-added a loading page
-formatted html correctly
'''
# imports
import sys
from python_modules.progenycalc.Handler import Handler

# main

def main(argv=None):
    if argv == None:
        argv = sys.argv
    # work
    # call handler for webversion
    handy = Handler()
    return 0

if __name__ == "__main__":
    sys.exit(main())

