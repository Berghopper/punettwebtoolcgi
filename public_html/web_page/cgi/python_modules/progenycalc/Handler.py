# needs form, googlepie, input,form,  progenycalc, data
# add module dir
import sys
import cgi
sys.path.append('./python_modules/HTML')
sys.path.append('./python_modules/progenycalc')

from html_data import *
from python_modules.HTML import *
from Progenycalculator import ProgenyCalculator
from GooglePie import GooglePie

class Handler():
    '''
    this class handles the dynamic part of the webpage
    inputs and forms etc
    '''
    def __init__(self):
        '''
        initiate class
        '''
        # define form
        self.form = cgi.FieldStorage()
        # if no mominp or dadinp --> parentform
        self.mominp = self.form.getvalue('Mother')
        self.dadinp = self.form.getvalue('Father')
        self.loadingbar = self.form.getvalue('loadingbar')
        if not self.mominp or not self.dadinp:
            self.parentform()
        # else, continue with labelform
        else:
            # define calcy for functions
            self.calcy = ProgenyCalculator(self.mominp, self.dadinp, \
            None, self, True)
            # if not all labelinps --> labelform
            if not self.check_labels():
                self.labelform()
                handy.basic_error_print("Please fill in all forms", \
                tabs=2)
            # else, continue
            else:
                # if not loadingbar = true, load loadingbar
                if not self.loadingbar:
                    self.loading_page()
                # else, load pie chart
                else:
                    self.have_some_pie()
        print(web_end)

    def parentform(self):
        """
        parentform handles the input of genotype of both parents
        """
        # define form
        startform = Form('startform')
        startform.addAttributes({'name':'Startform', \
        'action':sys.argv[0].split("/")[-1], 'method':'post'})
        # define inputs
        mom = Input('text')
        mom.addAttributes({'name':'Mother', 'placeholder':'e.g.; AaBb',
        'maxlength':'16'})
        dad = Input('text')
        dad.addAttributes({'name':'Father', 'placeholder':'e.g.; AaBb',
        'maxlength':'16'})
        sub = Input('submit')
        sub.addAttribute('value', 'Submit')
        content = '''
        \t\t<p class="p">Fill in the genotypes of parents:</p>\n'''
        # update inside
        startform.addInside(content)
        startform.addInput(mom, tabs=3)
        startform.addInput(dad, tabs=3)
        startform.addInput(sub, tabs=3)
        startform.addInside('\t\t\t<br/><p class="p">Please fill'\
        ' in both fields.</p>\n\t\t')

        # print form out on page
        print(web_begin)
        print(description)
        print('\t\t'+startform.tag)

    def check_labels(self):
        '''
        checks if labels are given, returns false if not
        '''
        self.label_inputs = []
        # for each gene, define and get inputs
        for gene in self.calcy.mom.genes:
            big = gene[0].upper()
            tiny = gene[0].lower()
            a_gene = big+tiny
            dom = self.form.getvalue(big)
            rec = self.form.getvalue(tiny)
            gen = self.form.getvalue(a_gene)
            # if one of the inputs isn't filled in, print labelform
            # again
            if not dom or not rec or not gen:
                return False
            else:
                # else, append inputs to a label list that will be
                # joined later
                self.label_inputs.append(a_gene+'='+gen+': '+big+'='+\
                dom+', '+tiny+'='+rec+';')
        self.label_inputs = "".join(self.label_inputs)
        if not self.label_inputs:
            return False
        else:
            return True

    def labelform(self):
        '''
        Labelform is used for printing out the form for traits of given
        genotypes.
        '''
        # define form
        labelform = Form('labelform')
        labelform.addAttributes({'name':'Labelform', \
        'action':sys.argv[0].split("/")[-1], 'method':'get'})
        # define inputs
        labelform.addInside('\n\t\t\t<p class="p">Fill in the names of'+
        ' the given traits:</p>\n')
        # for each gene that there is, add inputs:
        # Aa, A and a
        for gene in self.calcy.mom.genes:
            # define loose args
            big = gene[0].upper()
            tiny = gene[0].lower()
            a_gene = big+tiny
            # make them into inputs
            i_gene = Input('text')
            i_gene.addAttributes({'name':str(a_gene), \
            'placeholder':'e.g.; Haircolor'})
            i_tiny = Input('text')
            i_tiny.addAttributes({'name':str(tiny), \
            'placeholder':'e.g.; Blonde'})
            i_big = Input('text')
            i_big.addAttributes({'name':str(big), \
            'placeholder':'e.g.; Black'})
            # add all inputs
            labelform.addInput(i_gene, tabs=3)
            labelform.addInput(i_big, tabs=3)
            labelform.addInput(i_tiny, tabs=3)

        # make hidden fields to parse through mom, dad and loading time
        # loading time is parsed to a piece of js that uses it to
        # tell a percentage to increase each x seconds
        mom_parse = Input('hidden')
        mom_parse.addAttributes({'name':'Mother', 'value':self.mominp})
        dad_parse = Input('hidden')
        dad_parse.addAttributes({'name':'Father', 'value':self.dadinp})
        time_parse = Input('hidden')
        time_parse.addAttributes({'name':'load_time', 'value':\
        str(load_timer[self.calcy.comb_len/2])})
        # add them
        labelform.addInput(mom_parse, tabs=3)
        labelform.addInput(dad_parse, tabs=3)
        labelform.addInput(time_parse, tabs=3)
        # add button
        sub = Input('submit')
        sub.addAttribute('value', 'Submit')
        labelform.addInput(sub, tabs=3)
        labelform.addInside('\t\t')

        # print form
        print(web_begin)
        print('\t\t'+labelform.tag)

    def have_some_pie(self):
        '''Aa=haircolor: a=blonde, A=brown; Bb...'''
        # update the calcy object
        self.calcy = ProgenyCalculator(self.mominp, self.dadinp, \
        self.label_inputs[:-1], self, True)
        genotype_data, fenotype_data = self.calcy.finished_data
        # google chart
        googy = GooglePie()
        # make genotype chart
        googy.addChart("drawChart0", "Genotypes", \
        "Chance of getting genotype", genotype_data)
        # make fenotype chart
        googy.addChart("drawChart1", "Fenotypes", \
        "Chance of getting Fenotype", fenotype_data, dnatype='fen')
        # print out the charts and some basic lines
        print(web_begin)
        self.tag_print('Calculation duration: '+\
        str(self.calcy.calculation_time)+' s', tabs=2)
        self.tag_print('Results of:', tabs=2)
        self.tag_print('Mother: {} | Father: {}'.format(\
        self.calcy.mom.genotype, self.calcy.dad.genotype), tabs=2)
        print(googy)

    def loading_page(self):
        '''
        the loading page calls a piece of js to make a loading animation
        most of the animation is made with css, the javascript just
        increases the percentage
        '''
        loading_page = web_begin_load+'_load" class="defaultdiv">'+\
        '''\n\t\t<div class="loader">
        \t\t<div class="loader__figure">\n\t\t\t</div>
        \t\t<p id="loader_an" class="loader__label">'''+\
        '''0% loading, please wait...</p>\n\t\t</div>'''
        # make an invisible form to parse all previous inputs to google
        # pie chart, also update loader to be true so google pie chart
        # gets called.
        invisform = Form('invisform')
        invisform.addAttributes({'name':'invisform', \
        'action':sys.argv[0].split("/")[-1], 'method':'post'})
        # make mom, dad, loadingbar as hidden vals
        mom_parse = Input('hidden')
        mom_parse.addAttributes({'name':'Mother', 'value':self.mominp})
        dad_parse = Input('hidden')
        dad_parse.addAttributes({'name':'Father', 'value':self.dadinp})
        loadbar = Input('hidden')
        loadbar.addAttributes({'name':'loadingbar', 'value':'True'})
        # add all previous inputs
        invisform.addInside('\n')
        invisform.addInput(mom_parse, tabs=3)
        invisform.addInput(dad_parse, tabs=3)
        invisform.addInput(loadbar, tabs=3)
        # make all parse_label inputs and add them
        for gene in self.calcy.mom.genes:
            big = gene[0].upper()
            tiny = gene[0].lower()
            a_gene = big+tiny
            i_gene = Input('hidden')
            i_gene.addAttributes({'name':a_gene, \
            'value':str(self.form.getvalue(a_gene))})
            i_tiny = Input('hidden')
            i_tiny.addAttributes({'name':tiny, \
            'value':str(self.form.getvalue(tiny))})
            i_big = Input('hidden')
            i_big.addAttributes({'name':big, \
            'value':str(self.form.getvalue(big))})
            # add all inputs
            invisform.addInput(i_gene, tabs=3)
            invisform.addInput(i_big, tabs=3)
            invisform.addInput(i_tiny, tabs=3)
        invisform.addInside('\t\t')
        # print page with custom ending for grabbing js
        print(loading_page)
        print('\t\t'+invisform.tag)
        web_end = \
        '''\t\t</div>
        </div>
        <!--
        grab own javascript
        -->
        <script type="text/javascript" src="../js/animation.js">
        </script>'''+\
        '\n\t</body>\n</html>'
        print(web_end)
        # exit so normal end doesn't get printed
        sys.exit()

    def tag_print(self, stringy, tabs=0):
        """
        used for printing a basic html p tag
        """
        print('\t'*tabs+'<p class="p">'+str(stringy)+'</p>')

    def basic_error_print(self, stringy, tabs=0):
        """
        used for printing an error html p tag
        """
        print('\t'*tabs+'<p class="errorp">'+str(stringy)+'</p>')

    def error_print(self, errorcode):
        '''
        print error codes with a dict
        error codes:
        1 = something went wrong
        2, 3, 6 = something wrong with parent
        4, 5, 7, 8, 9 = something wrong with calculating
        '''
        errorcodes = {
        1: '<p class="errorp">Oops!, something went wrong</p>'+web_end,
        2: [self.parentform, '<p class="errorp">One or more of the '\
           'genotypes are incorrect.</p>'+web_end],
        3: [self.parentform, '<p class="errorp">You cannot cross two adults of '\
            'the same gender!</p>'+web_end],
        4: '<p class="errorp">no match in gendict</p>'+web_end,
        5: '<p class="errorp">no match in fendict</p>'+web_end,
        6: [self.parentform, '<p class="errorp">The genotypes don\'t '\
           'correspond, make sure they do.</p>'+web_end],
        7: '<p class="errorp">error sorting labels, are you '\
            'sure your input is correct?</p>'+web_end,
        8: '<p class="errorp">error sorting labels, missing '\
           'or incorrect gene</p>'+web_end,
        9: '<p class="errorp">error sorting labels, missing '\
           'or incorrect traits</p>'+web_end
        }
        # check if the instance is a list or not
        if isinstance(errorcodes[errorcode], list):
            # if it is, run command and print error
                errorcodes[errorcode][0]()
                print(errorcodes[errorcode][1])
        else:
            # else just print the error
            print(errorcodes[errorcode])
