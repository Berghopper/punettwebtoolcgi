class GooglePie():
    '''
    this class is used for making a google pie chart object inside
    the webpage
    '''
    def __init__(self):
        '''
        initiates class
        '''
        # define static vars
        self.beg = '''
        <!--
        google pie chart
        -->
        <script type="text/javascript" src='''+\
        '''"https://www.gstatic.com/charts/loader.js">\n\t</script>
        <script type="text/javascript">'''+"""
          google.charts.load('current', {'packages':['corechart']});"""
        self.end = '''
        </script>'''
        self.charts = []
        self.objects = []

    def addChart(self, function_name, name, desc, data, dnatype='gen'):
        """
        cCn add a chart to the googlepie script and webpage
        """
        # when adding a chart, divide the content into a few pieces
        # first, seoncd, third and fourth. format them and put them
        # inside the chart
        first = "\n\t\tgoogle.charts.setOnLoadCallback({});\n\t\t"\
        "function {}()"\
        .format(function_name, function_name)+\
        '{\n\t\tvar data = google.visualization.arrayToDataTable(['+\
        "\n\t\t['"+name+"','"+desc+"'],\n"
        # depending on if genotype or fenotype, format the data in it
        if dnatype == 'gen':
            second = self.formatGenotype(data)
        else:
            second = self.formatFenotype(data)
        third = '\n\t\tvar options = {'+"title: '{}',".format(name)+\
        "is3D: true,};"
        fourth = "\n\t\tvar chart = new google.visualization.PieChart("\
        "document.getElementById('{}'));\n".format(name)+\
        "\t\tchart.draw(data, options);\n\t\t}"
        # append chart to charts and append the div for loading to
        # objects list.
        self.charts.append(first+second+third+fourth)
        self.objects.append('\n\t\t<div id={} class=google_pie></div>'\
        .format("'"+name+"'"))

    def formatGenotype(self, data): 
        '''
        formats genotype data for the google pie chart
        '''
        content = []
        for genotype in data:
            content.append("\t\t['"+genotype[0]+"',"+\
            str(genotype[1][0])+"],\n")
        return "".join(content)[:-2]+']);'

    def formatFenotype(self, data):
        '''
        formats fenotype data for the google pie chart
        '''
        content = []
        for fenotype in data:
            content.append("\t\t['"+fenotype[0]+" | traits: "\
            +fenotype[1][2]+"',"+str(fenotype[1][0])+"],\n")
        return "".join(content)[:-2]+']);'

    def __str__(self):
        '''
        return for print statements, makes everthing a bit easier to 
        print
        '''
        return self.beg+" ".join(self.charts)+self.end+"<br/>"\
        .join(self.objects)
