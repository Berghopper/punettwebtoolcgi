# constants
web_begin = '''Content-type:text/html\r\n\r\n
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Punett concept page</title>
<!--
        define charset
-->
        <meta http-equiv="Content-Type" content="text/html;
        charset=UTF-8" />
<!--
        define css sheets
-->
        <link rel="stylesheet" type="text/css"
        href="../css/main_style.css"/>
        <link rel="stylesheet" type="text/css"
        href="../css/animation.css"/>
    </head>
    <body>
<!--
        main body, has content of webpage
-->
        <div id="main" class="defaultdiv">
            <h1 class="title1">Casper Peters @ Bio-Inf</h1>
            <img alt="school banner" src="../objects/images/school_hub.png"
            class="page-img"/>
<!--
            dropdown menu
-->
            <div id="nav">
                <div id="nav_wrapper">
                    <ul>
                        <li><a href="../home.html">Home</a></li><li>
                        <a href="javascript:void(0)">Hubs <img
                        alt="hamburger menu icon" class="ham_img"
                        src="../objects/images/hamburger.png"/></a>
                            <ul>
                                <li><a href="../html/school_hub.html">
                                School</a></li>
                                <li><a href="../html/tinker_hub.html">
                                Tinkering</a></li>
                            </ul>
                        </li><li>
                        <a href="javascript:void(0)">Themes <img
                        alt="hamburger menu icon" class="ham_img"
                        src="../objects/images/hamburger.png"/></a>
                            <ul>
                                <li><a href="../html/themes/4/theme_hub.html">
                                Theme 4</a></li>
                            </ul>
                        </li><li>
                        <a href="../html/cv.html">C.V.</a></li>
                    </ul>
                </div>
                <div id="clear"></div>
            </div>
<!--
            dropdown menu
-->
<!--
            child of main, mostly used for text
-->
            <div id="mainchild" class="defaultdiv">
'''

web_begin_load = """Content-type:text/html\r\n\r\n
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Punett concept page</title>
<!--
        define charset
-->
        <meta http-equiv="Content-Type" content="text/html;
        charset=UTF-8" />
<!--
        define css sheets
-->
        <link rel="stylesheet" type="text/css"
        href="../css/main_style.css"/>
        <link rel="stylesheet" type="text/css"
        href="../css/animation.css"/>
    </head>
    <body>
<!--
        main body, has content of webpage
-->
        <div id="main" class="defaultdiv">
            <h1 class="title1">Casper Peters @ Bio-Inf</h1>
            <img alt="school banner" src="../objects/images/school_hub.png"
            class="page-img"/>
<!--
            dropdown menu
-->
            <div id="nav">
                <div id="nav_wrapper">
                    <ul>
                        <li><a href="../home.html">Home</a></li><li>
                        <a href="javascript:void(0)">Hubs <img
                        alt="hamburger menu icon" class="ham_img"
                        src="../objects/images/hamburger.png"/></a>
                            <ul>
                                <li><a href="../html/school_hub.html">School</a>
                                </li>
                                <li><a href="../html/tinker_hub.html">Tinkering
                                </a></li>
                            </ul>
                        </li><li>
                        <a href="javascript:void(0)">Themes <img
                        alt="hamburger menu icon" class="ham_img"
                        src="../objects/images/hamburger.png"/></a>
                            <ul>
                                <li><a href="../html/themes/4/theme_hub.html">
                                Theme 4</a></li>
                            </ul>
                        </li><li>
                        <a href="../html/cv.html">C.V.</a></li>
                    </ul>
                </div>
                <div id="clear"></div>
            </div>
<!--
            dropdown menu
-->
<!--
            child of main, mostly used for text
-->
            <div id="mainchild"""

load_timer = {
1.0 : 0.00038,
2.0 : 0.00073,
3.0 : 0.00205,
4.0 : 0.00848,
5.0 : 0.03956,
6.0 : 0.17096,
7.0 : 0.74938,
8.0 : 3.37527,
9.0 : 15.72077,
10.0 : 69.12289
}

web_end = '''

            </div>
        </div>
    </body>
</html>
'''

description = """
        <h2 class="title2 u">Offspring Fenotype calculator</h2>
        <h3 class="title3">Description:</h3>
        <p class="p">This tool is used for calculating the probabilities of 
        possible fenotypes of an offspring with given genotypes of parents.<br/>
        For more information visit 
        <a href="../html/themes/4/progeny_info.html">this</a> 
        link or fill in the form below to calculate your fenotypes:</p>
        <p class="p">Input exmaple: "Mother: aAbb, Father: AaBb", 
        after this is entered correctly, a new set of forms will pop up and 
        ask you for 'properties' of certain allels.
        What we mean by this, is an inheritble tait, e.g. "Aa = haircolor". 
        Then "A" could be "brown" and "a" could be "blonde".
        </p>"""
