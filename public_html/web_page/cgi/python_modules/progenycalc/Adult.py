# add module dir
import sys
sys.path.append('./python_modules/DNA')
sys.path.append('./python_modules/progenycalc')

from Person import Person
from Child import Child


class Adult(Person):
    """
    This class is used for defining an adult (or parent) and stores
    basic genetic data and can combine itself with the other gender
    """
    def __init__(self, raw_string, handy, gender, html_state, prog_calc):
        """
        Initiates class adult by running the person Initiator,
        checking it's genotype and settings some variables
        """
        Person.__init__(self, raw_string, handy, gender, html_state, prog_calc)
        # if correct, define genotype and genes of self
        self.isValidGenotype()
        # define genes
        self.getGenes()

    def getGametes(self, sequences):
        """
        Grabs gametes from genotypes with the generated binairy sequences
        used as index numbers e.g. Aa[0] = A or Aa[1] = a
        thus:
        0  1
        AaBb
        --> Ab
        """
        self.gametes = []
        number_of_genes = int(len(self.genotype)/2)

        # with all the possible index sequences use it to index
        # raw_gametes
        for sequence in sequences:
            gamete = []
            for i, gen in enumerate(self.genes):
                allele = gen[int(sequence[i])]
                gamete.append(allele)
            gamete = "".join(gamete)
            self.gametes.append(gamete)

    def haveUnsafeSexWith(self, mate):
        """
        combines gametes into all possible genotypes possible with the
        help of class child and gamete
        """
        # first check if they're not the same sex
        if not self.gender == mate.gender:
            possible_children = []
            # for each gamete in mom and for each gamete in dad,
            # combine them into a single combination
            for s_gamete in self.gametes:
                for m_gamete in mate.gametes:
                    # call child and append to possible children
                    possible_children.append(Child(s_gamete, m_gamete, \
                    self.handy, self.html_state, self.prog_calc))
            # return children to Progenycalc
            return possible_children
        else:
        # if self and mate are the same sex, exit program and tell the
        # user they can't reproduce.
            if self.html_state:
                sys.exit(self.handy.error_print(3))
            else:
                sys.exit(self.prog_calc.error_print(3))
