import re
from .PairedTag import PairedTag

class Form(PairedTag):
    """This class uses PairedTag class to define a Form object"""
    def __init__(self, f_id):
        """
        inits Pairedtag and defined form id
        """
        PairedTag.__init__(self, 'form')
        self.addAttribute('id', f_id)

    def addInput(self, input_obj, tabs=0):
        """
        This function adds an input to the form and saves it in
        self.inputs
        """
        # define tagstring
        tabstring = '\t'*tabs
        # unpack object
        inptag = input_obj.tag
        # search for input type and name
        type_match = re.search('type="text"', inptag)
        name_match = re.search('name=".+"', inptag)
        # if both were not found, don't give it a p tag
        if not type_match or not name_match:
            self.addInside(tabstring+inptag+"<br/>\n")
        else:
            self.addInside(tabstring+'<p class="inp">'+name_match.group()\
            .split("=")[1].split(" ")[0].replace('"','')+\
            ':</p>'+inptag+"<br/>\n")
