class PairedTag():
    """
    This class defines a paired html tag
    """
    def __init__(self, tag):
        """
        make basic tag
        """
        self.begin = '<'+tag+' >'
        self.end = '</'+tag+'>'
        self.inside = ''
        self.at_info = {}
        self.tag = self.begin+self.inside+self.end

    def addAttribute(self, attribute, at_value):
        """
        Add an attribute inside the tag
        """
        self.at_info[attribute] = at_value
        self.updateTag()

    def addAttributes(self, attributes):
        """
        Add attributes inside the tag
        input = dict
        """
        for at, val in attributes.items():
            self.addAttribute(at, val)

    def updateTag(self):
        """
        Updates tag attributes
        """
        atts = []
        for at, at_val in self.at_info.items():
            atts.append(at+'='+'"'+at_val+'"')
        # redefine self.tag
        self.tag = self.begin[:-1]+" ".join(atts)+'>'+self.inside+\
        self.end

    def addInside(self, content):
        """
        puts something in between the tag
        """
        self.inside += content
        self.updateTag()
