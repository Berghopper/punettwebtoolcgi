class UnpairedTag():
    """
    This class defines an unpaired html tag
    """
    def __init__(self, tag):
        """
        make basic tag
        """
        self.base_tag = '<'+tag+' />'
        self.tag = '<'+tag+' />'
        self.at_info = {}

    def addAttribute(self, attribute, at_value):
        """
        Add an attrivute inside the tag
        """
        self.at_info[attribute] = at_value
        self.updateTag()

    def addAttributes(self, attributes):
        """
        Add attrivutes inside the tag
        input = dict
        """
        for at, val in attributes.items():
            self.addAttribute(at, val)

    def updateTag(self):
        """
        Updates tag attributes
        """
        atts = []
        for at, at_val in self.at_info.items():
            atts.append(at+'='+'"'+at_val+'"')
        # redefine self.tag
        self.tag = self.base_tag[:-2]+" ".join(atts)+'/>'
