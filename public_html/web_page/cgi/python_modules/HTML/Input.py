from .UnpairedTag import UnpairedTag

class Input(UnpairedTag):
    """This class uses PairedTag class to define a Input object"""
    def __init__(self, i_type):
        """
        inits Unpairedtag and defined input type
        """
        UnpairedTag.__init__(self, 'input')
        self.addAttribute('type', i_type)
